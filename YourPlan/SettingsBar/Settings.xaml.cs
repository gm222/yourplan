﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourPlan.Klasy;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace YourPlan.SettingsBar
{
    public sealed partial class Settings : UserControl
    {
        Ustawienia ust;

        public Settings(object obj)
        {
            this.InitializeComponent();
            ust = obj as Ustawienia;
            this.DataContext = ust;

            PN.Checked      += PNCH;
            PN.Unchecked    += PNCH;

            WT.Checked      += PNCH;
            WT.Unchecked    += PNCH;

            SR.Checked      += PNCH;
            SR.Unchecked    += PNCH;

            CZ.Checked      += PNCH;
            CZ.Unchecked    += PNCH;

            PT.Checked      += PNCH;
            PT.Unchecked    += PNCH;

            SB.Checked      += PNCH;
            SB.Unchecked    += PNCH;

            NI.Checked      += PNCH;
            NI.Unchecked    += PNCH;
            
        }

        private async void PNCH(object sender, RoutedEventArgs e)
        {
            var x = (sender as CheckBox).IsChecked;

            short liczba = 0;

            if (x == true)
                liczba = 200;

            
            switch ((sender as CheckBox).Name)
            {
                case "PN":
                    ust.KolumnaPoM = liczba;
                break;
                case "WT":
                    ust.KolumnaWtM = liczba;
                break;

                case "SR":
                    ust.KolumnaSrM = liczba;
                break;
                case "CZ":
                    ust.KolumnaCzM = liczba;
                break;
                case "PT":
                    ust.KolumnaPtM = liczba;
                break;
                case "SB":
                    ust.KolumnaSbM = liczba;
                break;
                case "NI":
                    ust.KolumnaNM = liczba;
                break;
            }

            await SaveData("Settings", ust);
        }

        private async Task<bool> SaveData(String name, Ustawienia obj)
        {

            StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync(name,
                                                                CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);

            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                Type typ = obj.GetType();
                XmlSerializer serializer = new XmlSerializer(typ);
                serializer.Serialize(outStream.AsStreamForWrite(), obj);
                //serializer.WriteObject(outStream.AsStreamForWrite(), obj);
                await outStream.FlushAsync();
            }
            raStream.Dispose();
            return true;
        }
        Brush stara        = null;
        Button zaznaczony   = null;
        short jaki = 0;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (zaznaczony != null)
                zaznaczony.BorderBrush = stara;

            kolor.Visibility = Windows.UI.Xaml.Visibility.Visible;

            stara = (sender as Button).BorderBrush;
            (sender as Button).BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 0));
            zaznaczony = sender as Button;
            var x = zaznaczony.Background as SolidColorBrush;
            color.Color = x.Color;

            switch (zaznaczony.Name)
            {
                case "cw":
                    jaki = 1;
                break;
                case "lab":
                    jaki = 2;
                break;
                case "wyk":
                    jaki = 3;
                break;
                case "sem":
                    jaki = 4;
                break;
                case "inn":
                    jaki = 5;
                break;
            }

        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            switch (jaki)
            {
                case 1:
                    ust.Cwiczenia = new SolidColorBrush(color.Color);
                break;
                case 2:
                    ust.Labolatoria = new SolidColorBrush(color.Color);
                break;
                case 3:
                    ust.Wyklad = new SolidColorBrush(color.Color);
                break;
                case 4:
                    ust.Seminaria = new SolidColorBrush(color.Color);
                break;
                case 5:
                    ust.Inne = new SolidColorBrush(color.Color);
                break;

            }

            await ust.Kolory();

            await SaveData("Settings", ust);

            jaki = 0;

            zaznaczony.BorderBrush = stara;

            zaznaczony = null;
            stara = null;
            kolor.Visibility = Windows.UI.Xaml.Visibility.Collapsed;



        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            jaki = 0;
            kolor.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            zaznaczony.BorderBrush = stara;

            zaznaczony = null;
            stara = null;
        }

    }

    public class ColorToBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
