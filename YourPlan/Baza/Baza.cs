﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SQLite;

namespace YourPlan.Baza
{

    /// Tabele
    class PlanL
    {
        [SQLite.AutoIncrement, SQLite.PrimaryKey]
        public int id_P        { get; set; } //autoinc id Planu
        public short Dzien      { get; set; } //jaki dzien 1=pon 7=niedz
        public int id_NP       { get; set; } //id Pracownika
        public DateTime Start   { get; set; } //o której godz start
        public DateTime Stop    { get; set; } //o której godz koniec
        public int id_Pokoju   { get; set; } //jaka sala
        public short typ        { get; set; } //jaki rodzaj zajęć
        public int id_K        { get; set; } //jaki kierunek
        public int id_Przed    { get; set; } //id przedmiotu
        public int Margines { get; set; }
        public int Height { get; set; }
    }

    class Pracownik
    {
        [SQLite.AutoIncrement, SQLite.PrimaryKey]
        public int id_NP       { get; set; }
        public String NazwaP    { get; set; }
    }

    class Kierunek
    {
        [SQLite.AutoIncrement, SQLite.PrimaryKey]
        public int id_K        { get; set; }
        public String NazwaK    { get; set; }
    }

    class Przedmiot
    {
        [SQLite.AutoIncrement, SQLite.PrimaryKey]
        public int id_Przed    { get; set; }
        public String NazwaP    { get; set; }
    }

    class Sala
    {
        [SQLite.AutoIncrement, SQLite.PrimaryKey]
        public int id_Pokoju   { get; set; }
        public String NazwaS    { get; set; }
    }

    class SQL
    {
        private String _path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "data.db3");
        private SQLiteAsyncConnection _con;

        //zrobić usuwanie przedmiotu z bazy, aktualizacja przedmiotu w bazie !!

        private async void Connection()
        {
            _con = new SQLiteAsyncConnection(_path);
            var result = await _con.CreateTablesAsync<PlanL, Pracownik, Kierunek, Przedmiot, Sala>();
        }

        public SQL()
        {
            Connection();
        }

        public async Task<bool> InsertPlan(PlanL plan)
        {
            var result = await _con.InsertAsync(plan);

            if (result == 1)
                return true;
            else
                return false;

        }

        public async Task<List<PlanL>> GetPlanL(DateTime warunek)
        {
            if (warunek != null)
            {
                var query = await _con.Table<PlanL>().Where(x => x.Stop < warunek).ToListAsync();

                if (query == null)
                    return null;
                else
                    return query;
            }
            else
                return await _con.Table<PlanL>().ToListAsync();
        }

        public async Task<bool> DeletePlan(PlanL plan)
        {
           var result = await _con.DeleteAsync(plan);

           if (result == 1)
               return true;
           else
               return false;
        }

        public async Task<bool> UpdatePlan(PlanL plan)
        {
            var result = await _con.UpdateAsync(plan);

            if (result == 1)
                return true;
            else
                return false;
        }

        public async Task<bool> InsertPracownik(String prac)
        {
            var query = await _con.Table<Pracownik>().Where(x => x.NazwaP == prac).FirstOrDefaultAsync();

            if (query == null)
            {
                var obj = new Pracownik() { NazwaP = prac };

                var result = await _con.InsertAsync(obj);

                if (result == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public async Task<bool> InsertKierunek(String kier)
        {
            var query = await _con.Table<Kierunek>().Where(x => x.NazwaK == kier).FirstOrDefaultAsync();

            if (query == null)
            {
                var obj = new Kierunek() { NazwaK = kier };

                var result = await _con.InsertAsync(obj);

                if (result == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;

        }

        public async Task<bool> InsertPrzedmiot(String przed)
        {
            var query = await _con.Table<Przedmiot>().Where(x => x.NazwaP == przed).FirstOrDefaultAsync();

            if (query == null)
            {
                var obj = new Przedmiot() { NazwaP = przed };

                var result = await _con.InsertAsync(obj);

                if (result == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public async Task<List<Przedmiot>> GetPrzedmiot()
        {
            var query = await _con.Table<Przedmiot>().ToListAsync();

            if (query == null)
                return null;

            return query;
        }

        public async Task<List<Pracownik>> GetNauczyciel()
        {
            var query = await _con.Table<Pracownik>().ToListAsync();

            if (query == null)
                return null;

            return query;
        }

        public async Task<List<Sala>> GetSala()
        {
            var query = await _con.Table<Sala>().ToListAsync();

            if (query == null)
                return null;

            return query;
        }

        

        public async Task<bool> InsertSala(String sala)
        {
            var query = await _con.Table<Sala>().Where(x => x.NazwaS == sala).FirstOrDefaultAsync();

            if (query == null)
            {
                var obj = new Sala() { NazwaS = sala };

                var result = await _con.InsertAsync(obj);

                if (result == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }





        


    }

    

}
