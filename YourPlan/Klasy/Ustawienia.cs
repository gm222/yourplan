﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace YourPlan.Klasy
{
    [XmlRootAttribute("Ustawienia", Namespace = "", IsNullable = false)]
    public class Ustawienia : INotifyPropertyChanged
    {

        public static int dom                   =   200;
        public static GridLength domW           =   new GridLength(1, GridUnitType.Star);
        public static GridLength domWZ          =   new GridLength(0);

        //pola prywatne

        private SolidColorBrush _cwiczenia = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 32,32));
        private SolidColorBrush _labolatoria = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0,234,59));
        private SolidColorBrush _wyklad = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0,127,252));
        private SolidColorBrush _seminaria = new SolidColorBrush(Windows.UI.Color.FromArgb(255,195,232,0));
        private SolidColorBrush _inne = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 115,115,115));

        

        ///////////////////////////////////

        private double _kolumnaPoM              =   dom;
        private double _kolumnaWtM              =   dom;
        private double _kolumnaSrM              =   dom;
        private double _kolumnaCzM              =   dom;
        private double _kolumnaPtM              =   dom;
        private double _kolumnaSbM              =   dom;
        private double _kolumnaNM               =   dom;

        ///////////////////////////////////

        private GridLength ponW                 =   domW;
        private GridLength wtW                  =   domW;
        private GridLength srW                  =   domW;
        private GridLength czW                  =   domW;
        private GridLength ptW                  =   domW;
        private GridLength sbW                  =   domW;
        private GridLength nW                   =   domW;

        ///////////////////////////////////

        private bool ponV                       =   true;
        private bool wtV                        =   true;
        private bool srV                        =   true;
        private bool czV                        =   true;
        private bool ptV                        =   true;
        private bool sbV                        =   true;
        private bool nV                         =   true;


        //property publiczne
        [XmlIgnore()]
        public SolidColorBrush Cwiczenia    { get { return this._cwiczenia;     } set { this._cwiczenia     =   value;  ByteCwiczenia[0] = _cwiczenia.Color.A; ByteCwiczenia[1] = _cwiczenia.Color.R; ByteCwiczenia[2] = _cwiczenia.Color.G; ByteCwiczenia[3] = _cwiczenia.Color.B; OnPropertyChanged(); } }
        [XmlIgnore()]
        public SolidColorBrush Labolatoria  { get { return this._labolatoria;   } set { this._labolatoria   =   value;  ByteLabolatoria[0] = _labolatoria.Color.A; ByteLabolatoria[1] = _labolatoria.Color.R; ByteLabolatoria[2] = _labolatoria.Color.G; ByteLabolatoria[3] = _labolatoria.Color.B; OnPropertyChanged(); } }
         [XmlIgnore()]
        public SolidColorBrush Wyklad       { get { return this._wyklad;        } set { this._wyklad        =   value;  ByteWyklad[0] = _wyklad.Color.A; ByteWyklad[1] = _wyklad.Color.R; ByteWyklad[2] = _wyklad.Color.G; ByteWyklad[3] = _wyklad.Color.B; OnPropertyChanged(); } }
         [XmlIgnore()]
         public SolidColorBrush Seminaria   { get { return this._seminaria;     } set { this._seminaria     =   value;  ByteSeminaria[0] = _seminaria.Color.A; ByteSeminaria[1] = _seminaria.Color.R; ByteSeminaria[2] = _seminaria.Color.G; ByteSeminaria[3] = _seminaria.Color.B; OnPropertyChanged(); } }
         [XmlIgnore()]
         public SolidColorBrush Inne        { get { return this._inne;          } set { this._inne          =   value;  ByteInne[0] = _inne.Color.A; ByteInne[1] = _inne.Color.R; ByteInne[2] = _inne.Color.G; ByteInne[3] = _inne.Color.B; OnPropertyChanged(); } }
       
        ///////////////////////////////////
        [XmlAttributeAttribute()]
        public double KolumnaPoM            { get { return this._kolumnaPoM;    } 
                                              set {
                                                  this._kolumnaPoM    =   value;
                                                  switch ((int)_kolumnaPoM)
                                                  {
                                                      case 0:
                                                          this.PonW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.PonW = domW;
                                                      break;
                                                  };
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        [XmlAttributeAttribute()]
        public double KolumnaWtM
        {
            get { return this._kolumnaWtM; }
            set
            {
                this._kolumnaWtM = value;
                switch ((int)_kolumnaWtM)
                {
                    case 0:
                        this.WtW = domWZ;
                        break;
                    case 200:
                        this.WtW = domW;
                        break;
                }

                OnPropertyChanged();
            }
        }
        [XmlAttributeAttribute()]
        public double KolumnaSrM            { get { return this._kolumnaSrM;    } 
                                              set {
                                                  this._kolumnaSrM    =   value;
                                                  switch ((int)_kolumnaSrM)
                                                  {
                                                      case 0:
                                                          this.SrW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.SrW = domW;
                                                      break;
                                                  }
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        [XmlAttributeAttribute()]
        public double KolumnaCzM            { get { return this._kolumnaCzM;    } 
                                              set {
                                                  this._kolumnaCzM    =   value;
                                                  switch ((int)_kolumnaCzM)
                                                  {
                                                      case 0:
                                                          this.CzW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.CzW = domW;
                                                      break;
                                                  }
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        [XmlAttributeAttribute()]
        public double KolumnaPtM            { get { return this._kolumnaPtM;    } 
                                              set {
                                                  this._kolumnaPtM    =   value;
                                                  switch ((int)_kolumnaPtM)
                                                  {
                                                      case 0:
                                                          this.PtW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.PtW = domW;
                                                      break;
                                                  }
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        [XmlAttributeAttribute()]
        public double KolumnaSbM            { get { return this._kolumnaSbM;    } 
                                              set {
                                                  this._kolumnaSbM    =   value;
                                                  switch ((int)_kolumnaSbM)
                                                  {
                                                      case 0:
                                                          this.SbW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.SbW = domW;
                                                      break;
                                                  }
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        [XmlAttributeAttribute()]
        public double KolumnaNM             { get { return this._kolumnaNM;    } 
                                              set {
                                                  this._kolumnaNM    =   value;
                                                  switch ((int)_kolumnaNM)
                                                  {
                                                      case 0:
                                                          this.NW = domWZ;
                                                      break;
                                                      case 200:
                                                        this.NW = domW;
                                                      break;
                                                  }
                                                         
                                                  OnPropertyChanged(); 
                                                  } 
                                            }
        
        ///////////////////////////////////
         [XmlIgnore()]
        public GridLength PonW              { get { return this.ponW;           } set { this.ponW           =   value; OnPropertyChanged(); } }
         [XmlIgnore()]
        public GridLength WtW               { get { return this.wtW;            } set { this.wtW            =   value; OnPropertyChanged(); } }
         [XmlIgnore()]
        public GridLength SrW               { get { return this.srW;            } set { this.srW            =   value; OnPropertyChanged(); } }
         [XmlIgnore()]
        public GridLength CzW               { get { return this.czW;            } set { this.czW            =   value; OnPropertyChanged(); } }
        [XmlIgnore()]
        public GridLength PtW               { get { return this.ptW;            } set { this.ptW            =   value; OnPropertyChanged(); } }
         [XmlIgnore()]
        public GridLength SbW               { get { return this.sbW;            } set { this.sbW            =   value; OnPropertyChanged(); } }
         [XmlIgnore()]
        public GridLength NW                { get { return this.nW;             } set { this.nW             =   value; OnPropertyChanged(); } }

        ///////////////////////////////////
        [XmlAttributeAttribute()]
        public bool PonV                    { get { return this.ponV;           } set { this.ponV           =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool WtV                     { get { return this.wtV;            } set { this.wtV            =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool SrV                     { get { return this.srV;            } set { this.srV            =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool CzV                     { get { return this.czV;            } set { this.czV            =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool PtV                     { get { return this.ptV;            } set { this.ptV            =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool SbV                     { get { return this.sbV;            } set { this.sbV            =   value; OnPropertyChanged(); } }
        [XmlAttributeAttribute()]
        public bool NV                      { get { return this.nV;             } set { this.nV             =   value; OnPropertyChanged(); } }
        
        ///////////////////////////////////
        [XmlAttributeAttribute()]
        public List<byte> ByteInne = new List<byte>();
        [XmlAttributeAttribute()]
        public List<byte> ByteCwiczenia = new List<byte>();
        [XmlAttributeAttribute()]
        public List<byte> ByteLabolatoria = new List<byte>();
        [XmlAttributeAttribute()]
        public List<byte> ByteWyklad = new List<byte>();
        [XmlAttributeAttribute()]
        public List<byte> ByteSeminaria = new List<byte>();

        public event PropertyChangedEventHandler PropertyChanged;

        ///////////////////////////////////

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Domyslne()
        {

            ByteInne = new List<byte>
            {
               255, 115,115,115
            };

            ByteCwiczenia = new List<byte>
            {
                255, 255, 32,32
            };

            ByteLabolatoria = new List<byte>
            {
                255, 0,234,59
            };
 
            ByteWyklad = new List<byte>
            {
               255, 0,127,252
            };

            ByteSeminaria = new List<byte>
            {
                255,195,232,0
            };

        }

        public async Task<bool> Kolory()
        {
            ByteSeminaria[0] = Seminaria.Color.A;
            ByteSeminaria[1] = Seminaria.Color.R;
            ByteSeminaria[2] = Seminaria.Color.G;
            ByteSeminaria[3] = Seminaria.Color.B;

            ByteInne[0] = Inne.Color.A;
            ByteInne[1] = Inne.Color.R;
            ByteInne[2] = Inne.Color.G;
            ByteInne[3] = Inne.Color.B;

            ByteLabolatoria[0] = Labolatoria.Color.A;
            ByteLabolatoria[1] = Labolatoria.Color.R;
            ByteLabolatoria[2] = Labolatoria.Color.G;
            ByteLabolatoria[3] = Labolatoria.Color.B;

            ByteWyklad[0] = Wyklad.Color.A;
            ByteWyklad[1] = Wyklad.Color.R;
            ByteWyklad[2] = Wyklad.Color.G;
            ByteWyklad[3] = Wyklad.Color.B;

            ByteCwiczenia[0] = Cwiczenia.Color.A;
            ByteCwiczenia[1] = Cwiczenia.Color.R;
            ByteCwiczenia[2] = Cwiczenia.Color.G;
            ByteCwiczenia[3] = Cwiczenia.Color.B;

            return true;
        }

        public void Przypisz()
        {
            KolumnaPoM = KolumnaPoM;
            KolumnaWtM = KolumnaWtM;
            KolumnaSrM = KolumnaSrM;
            KolumnaCzM = KolumnaCzM;
            KolumnaPtM = KolumnaPtM;
            KolumnaSbM = KolumnaSbM;
            KolumnaNM  = KolumnaNM;

            _inne = new SolidColorBrush(Windows.UI.Color.FromArgb(ByteInne[0], ByteInne[1], ByteInne[2], ByteInne[3]));
            _cwiczenia = new SolidColorBrush(Windows.UI.Color.FromArgb(ByteCwiczenia[0], ByteCwiczenia[1], ByteCwiczenia[2], ByteCwiczenia[3]));
            _wyklad = new SolidColorBrush(Windows.UI.Color.FromArgb(ByteWyklad[0], ByteWyklad[1], ByteWyklad[2], ByteWyklad[3]));
            _labolatoria = new SolidColorBrush(Windows.UI.Color.FromArgb(ByteLabolatoria[0], ByteLabolatoria[1], ByteLabolatoria[2], ByteLabolatoria[3]));
            _seminaria = new SolidColorBrush(Windows.UI.Color.FromArgb(ByteSeminaria[0], ByteSeminaria[1], ByteSeminaria[2], ByteSeminaria[3]));

           
        }
    }
}
