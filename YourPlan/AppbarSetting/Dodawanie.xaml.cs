﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourPlan.Baza;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace YourPlan.AppbarSetting
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class Dodawanie : YourPlan.Common.LayoutAwarePage
    {
        SQL baza ;
        Grid gponiedzialek;
        Grid gwtorek;
        Grid gsroda;
        Grid gczwartek;
        Grid gpiatek;
        Grid gsobota;
        Grid gniedziela;



        public Dodawanie(object obj)
        {
            this.InitializeComponent();
      
            gl = obj as MainPage;

            gponiedzialek = gl.FindName("poniedzialek") as Grid;
            gwtorek = gl.FindName("wtorek") as Grid;
            gsroda = gl.FindName("sroda") as Grid;
            gczwartek = gl.FindName("czwartek") as Grid;
            gpiatek = gl.FindName("piatek") as Grid;
            gsobota = gl.FindName("sobota") as Grid;
            gniedziela = gl.FindName("niedziela") as Grid;

            List<string> godz = new List<string>();

            for (int i = 7; i < 22; i++)
            {
                DateTime s = new DateTime(2013, 5, 1, i, 0, 0);

                switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                {
                    case "en-US":
                    case "en-GB":
                        godz.Add(String.Format("{0}", s.ToString("h tt")));
                        break;
                    default:
                        godz.Add(String.Format("{0:HH}", s));
                        break;
                }
            }

            foreach (string h in godz)
            {
                godz1.Items.Add(h);
                godz2.Items.Add(h);
            }

            for (int i = 0; i < 60; i += 15)
            {
                min1.Items.Add(i);
                min2.Items.Add(i);
            }

            List<String> typprzed = new List<string>();
            List<String> dni = new List<string>();
            #region Typ zajęć i dni
            switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
            {
     
                case "de":
                    typprzed.Add("Vorlesung");
                    typprzed.Add("Übungen");
                    typprzed.Add("Laboratorium");
                    typprzed.Add("Seminar");
                    typprzed.Add("Andere");

                    dni.Add("Montag");
                    dni.Add("Dienstag");
                    dni.Add("Mittwoch");
                    dni.Add("Donnerstag");
                    dni.Add("Freitag");
                    dni.Add("Samstag");
                    dni.Add("Sonntag");


                break;
                case "pl":
                    typprzed.Add("Wykład");
                    typprzed.Add("Konwersatorium");
                    typprzed.Add("Labolatoria");
                    typprzed.Add("Seminaria");
                    typprzed.Add("Inne");

                    dni.Add("Poniedziałek");
                    dni.Add("Wtorek");
                    dni.Add("Środa");
                    dni.Add("Czwartek");
                    dni.Add("Piątek");
                    dni.Add("Sobota");
                    dni.Add("Niedziela");
                break;
                default: //ang
                    typprzed.Add("Lectures");
                    typprzed.Add("Workshops");
                    typprzed.Add("Laboratory");
                    typprzed.Add("Seminars");
                    typprzed.Add("Others");
                    
                    dni.Add("Monday");
                    dni.Add("Tuesday");
                    dni.Add("Wednesday");
                    dni.Add("Thursday");
                    dni.Add("Friday");
                    dni.Add("Saturday");
                    dni.Add("Sunday");
                break;
            }
            #endregion

            this.dzien.ItemsSource = dni;
            this.rodzaj.ItemsSource = typprzed;
        }

        MainPage gl;

        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState){}

       
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime start = new DateTime(2013, 5, 1, 8, 0, 0);
            DateTime stop = new DateTime(2013, 5, 1, 9, 0, 0);
            DateTime start2 = new DateTime(2013, 5, 1, 7, 0, 0);
            DateTime stop2 = new DateTime(2013, 5, 1, 8, 0, 0);
            int a = 0;
            a++;
            if (start2 >= start && start2 < stop)
            {
                a = 2;//nieprawda powiadomienie zajecia zaczynaja sie w trakcie innych
            }

            if (stop2 > start && start2 <= start)
            {
                a = 2; //błedy koncza
            }

            (gl.FindName("_frame") as UserControl).Content = null;
            (gl.FindName("glowny") as Grid).Margin = new Thickness(30, 0, 30, 10);
        }

        private async void pageRoot_Loaded(object sender, RoutedEventArgs e)
        {
           baza         =   new SQL();
/*
           var przed    =   (await baza.GetPrzedmiot()).Select(i => i.NazwaP).ToList();
           var sal      =   (await baza.GetSala()).Select(i => i.NazwaS).ToList();
           var nau      =   (await baza.GetNauczyciel()).Select(i => i.NazwaP).ToList();

           List<string> sprzed = new List<string>();
           foreach (var item in przed)
               sprzed.Add(item.NazwaP);
 
           przedmiot.ItemsSource    =   przed;
           nauczyciel.ItemsSource   =   nau;
           sala.ItemsSource         =   sal;
*/
        }

        private async void Button_Click1(object sender, RoutedEventArgs e)
        {

            Grid dane = new Grid();
            dane.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 180, 117));
            dane.Margin = new Thickness(5, 0, 5, 0);
            dane.Height = 150;
            dane.VerticalAlignment = VerticalAlignment.Top;

            ////////////////////////////////////////////////////////////////////
            Grid gorny = new Grid();
            gorny.VerticalAlignment = VerticalAlignment.Top;
            gorny.HorizontalAlignment = HorizontalAlignment.Stretch;

            TextBlock sala = new TextBlock();
            sala.Text = "Sala";
            sala.HorizontalAlignment = HorizontalAlignment.Center;
            sala.Margin = new Thickness(5);
            sala.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");
            sala.VerticalAlignment = VerticalAlignment.Top;

            TextBlock godzinaS = new TextBlock();
            godzinaS.Text = "GodzinaS";
            godzinaS.HorizontalAlignment = HorizontalAlignment.Left;
            godzinaS.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");
            godzinaS.Margin = new Thickness(5, 15, 5, 5);
            godzinaS.VerticalAlignment = VerticalAlignment.Top;

            gorny.Children.Add(sala);
            gorny.Children.Add(godzinaS);
            ///////////////////////////////////////////////////////////////////////

            StackPanel srodek = new StackPanel();
            srodek.VerticalAlignment = VerticalAlignment.Center;

            TextBlock naucz = new TextBlock();
            naucz.Text = "Wykładowca";
            naucz.FontSize = 20;
            naucz.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");
            naucz.TextAlignment = TextAlignment.Center;

            TextBlock przed = new TextBlock();
            przed.Text = "Przedmiot";
            przed.FontSize = 36;
            przed.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");
            przed.TextAlignment = TextAlignment.Center;

            srodek.Children.Add(naucz);
            srodek.Children.Add(przed);
            ////////////////////////////////////////////////////////////////////////
            TextBlock godzinaK = new TextBlock();
            godzinaK.Text = "GodzinaK";
            godzinaK.HorizontalAlignment = HorizontalAlignment.Left;
            godzinaK.FontFamily = new Windows.UI.Xaml.Media.FontFamily("Segoe UI");
            godzinaK.Margin = new Thickness(5, 5, 5, 15);
            godzinaK.VerticalAlignment = VerticalAlignment.Bottom;

            //Button usun = new Button();
            //usun.Content = "-";

            dane.Children.Add(gorny);
            dane.Children.Add(srodek);
            dane.Children.Add(godzinaK);

            gponiedzialek.Children.Add(dane);


            return;


            var p   =   this.przedmiot.Text;
            var r   =   this.rodzaj.SelectedIndex+1;
            var d   =   this.dzien.SelectedIndex +1;
            var gP  =   this.godz1.SelectedValue;
            var gK  =   this.godz2.SelectedValue;
            var mP  =   this.min1.SelectedValue;
            var mK  =   this.min2.SelectedValue;
            var n   =   this.nauczyciel.Text;
            var s   =   this.sala.Text;

            bool blad = false;

            if (p.Length == 0 || n.Length == 0 || s.Length == 0 || d == 0 || r == 0)
                blad = true;

            if (d == null || gP == null || gK == null || mP == null || mK == null)
                blad = true;

            if (blad == true)
            {
                switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                {
                    case "de":
                        await new Windows.UI.Popups.MessageDialog("Uzupełnij dane").ShowAsync();
                    break;
                    case "pl":
                        await new Windows.UI.Popups.MessageDialog("Uzupełnij dane").ShowAsync();
                    break;
                    default:
                        await new Windows.UI.Popups.MessageDialog("Uzupełnij dane").ShowAsync();
                    break;
                }

                return;
            }
            //zrobić globalną tablicę od Przedmiotu, prac itd i aktualizować ją jeśli bedzie true
            await baza.InsertPracownik(n);
            await baza.InsertPrzedmiot(p);
            await baza.InsertSala(s);
            

            var pr      =   await baza.GetNauczyciel();
            var sl      =   await baza.GetSala();
            var prze    =   await baza.GetPrzedmiot();

            var pr_id   =   (pr.Where(x => x.NazwaP == n).Select(i => i.id_NP).ToArray())[0];
           
            var sl_id   =   (sl.Where(x => x.NazwaS == s).Select(i => i.id_Pokoju).ToArray())[0];
            var prze_id =   (prze.Where(x => x.NazwaP == p).Select(i => i.id_Przed).ToArray())[0];

            var startD = new DateTime(2013,05,05, Convert.ToInt16(gP), Convert.ToInt16(mP),0);
            var stopD = new DateTime(2013,05,05, Convert.ToInt16(gK), Convert.ToInt16(mK), 0);

            if (stopD < startD)
            {
                Windows.UI.Popups.MessageDialog komunikat;
                switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                {
                    case "pl":
                        komunikat = new Windows.UI.Popups.MessageDialog("Bład w godzinie.", "Błąd");
                        break;
                    case "de":
                        komunikat = new Windows.UI.Popups.MessageDialog("Fehler im Zeit", "Fehlermeldung");
                        break;
                    default:
                        komunikat = new Windows.UI.Popups.MessageDialog("Mistake in time.", "Error");
                        break;
                }
                await komunikat.ShowAsync();
                return;
            }

            if ((stopD - startD).TotalMinutes < 30)
            {
                Windows.UI.Popups.MessageDialog komunikat;
                switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                {
                    case "pl":
                        komunikat = new Windows.UI.Popups.MessageDialog("Zajęcia nie mogą trwać krócej niż 30min.", "Błąd");
                        break;
                    case "de":
                        komunikat = new Windows.UI.Popups.MessageDialog("Der Unterricht kann nicht kürzer sein als 30 Minuten.", "Fehlermeldung");
                        break;
                    default:
                        komunikat = new Windows.UI.Popups.MessageDialog("Lectures can not be shorter than 30 minutes..", "Error");
                        break;
                }
                await komunikat.ShowAsync();
                return;
            }

            var przedmioty = await baza.GetPlanL(stopD);

            if (przedmioty == null)
            {//tutaj wstawiamy dane, bo nie ma takich przedmiotów co koliduje

                var margines = (Convert.ToInt32(gP) - 7) * 60 + Convert.ToInt32(mP);
                var wysokosc = (Convert.ToInt32(gK) - Convert.ToInt32(gP)) * 60 + (Convert.ToInt32(mK) - Convert.ToInt32(mP));

                var plan = new PlanL()
                {
                    Dzien = Convert.ToInt16(d),
                    id_K = 1,
                    id_Pokoju = sl_id,
                    id_Przed = prze_id,
                    Start = startD,
                    Stop = stopD,
                    id_NP = pr_id,
                    typ = Convert.ToInt16(r),
                    Margines = margines,
                    Height = wysokosc
                };

                var result = await baza.InsertPlan(plan);
                if (result == true)
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Zajęcia pomyślnie dodane ;)", "Sukces");
                            break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Unterricht erfolgreich hinzugefügt ;)", "Erfolg");
                            break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("Lectures successfully added.", "Success");
                            break;
                    }
                    await komunikat.ShowAsync();

                    //tutaj dodawanie
                    
                    /*
                    Grid dane = new Grid();
                    dane.Margin = new Thickness(5,margines,5,0);
                    dane.Height = wysokosc;
                    StackPanel stack = new StackPanel();
                    StackPanel gorny = new StackPanel();
                    TextBlock godzinaS = new TextBlock();
                    godzinaS.Text = "GodzinaS";
                    TextBlock sala = new TextBlock();
                    sala.Text = "Sala";
                    TextBlock naucz = new TextBlock();
                    naucz.Text = "nauczyciel";
                    TextBlock przed = new TextBlock();
                    przed.Text = "Przedmiot";
                    TextBlock godzinaK = new TextBlock();
                    godzinaK.Text = "godzinaK";
                    Button usun = new Button();
                    usun.Content = "-";
                    godzinaK.Height = przed.Height = naucz.Height = usun.Height = usun.Width = 30;
                    godzinaK.HorizontalAlignment = przed.HorizontalAlignment = godzinaK.HorizontalAlignment = HorizontalAlignment.Stretch;
                    gorny.Orientation = Orientation.Horizontal;
                    stack.VerticalAlignment = VerticalAlignment.Stretch;
                    stack.Height = 30;

                    gorny.Children.Add(godzinaS);
                    gorny.Children.Add(sala);
                    gorny.Children.Add(usun);
                    stack.Children.Add(gorny);
                    stack.Children.Add(naucz);
                    stack.Children.Add(przed);
                    stack.Children.Add(godzinaK);
                    dane.Children.Add(stack);

                    gponiedzialek.Children.Add(dane);
                    */

                    return;
                }
                else
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Coś poszło nie tak :(", "Błąd");
                            break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Etwas schief gegangen :(", "Fehlermeldung");
                            break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("something went wrong :(", "Error");
                            break;
                    }
                    await komunikat.ShowAsync();
                    return;
                }
            }
            else
            {
                var znaleziony1 = przedmioty.Where(x => startD >= x.Start && startD < x.Stop);

                if (znaleziony1 != null) //to błąd, wypisać komunikat
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch(Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Podane zajęcią kolidują z innym przedmiotem.", "Błąd");
                        break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Die gegebene Lektion stört mit einer anderen Lektion.", "Fehlermeldung");
                        break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("The given lesson collides with another lesson.", "Error");
                        break;
                    }
                    await komunikat.ShowAsync();
                    return;
                }

                znaleziony1 = przedmioty.Where(x => stopD > x.Start && startD <= x.Start);

                if (znaleziony1 != null) //to błąd, wypisać komunikat 
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Podane zajęcią kolidują z innym przedmiotem.", "Błąd");
                            break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Die gegebene Lektion stört mit einer anderen Lektion.", "Fehlermeldung");
                            break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("The given lesson collides with another lesson.", "Error");
                            break;
                    }
                    await komunikat.ShowAsync();
                    return;
                }

                var margines = (Convert.ToInt32(gP) - 7) * 60 + Convert.ToInt32(mP);
                var wysokosc = (Convert.ToInt32(gK) - Convert.ToInt32(gP)) * 60 + (Convert.ToInt32(mK) - Convert.ToInt32(mP));

                var plan = new PlanL()
                {
                    Dzien = Convert.ToInt16(d),
                    id_K = 1,
                    id_Pokoju = sl_id,
                    id_Przed = prze_id,
                    Start = startD,
                    Stop = stopD,
                    id_NP = pr_id,
                    typ = Convert.ToInt16(r),
                    Margines = margines,
                    Height = wysokosc
                };

                var result = await baza.InsertPlan(plan);
                if (result == true)
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Zajęcia pomyślnie dodane ;)", "Sukces");
                        break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Unterricht erfolgreich hinzugefügt ;)", "Erfolg");
                        break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("Lectures successfully added.", "Success");
                        break;
                    }
                    await komunikat.ShowAsync();
                    return;
                }
                else
                {
                    Windows.UI.Popups.MessageDialog komunikat;
                    switch (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride)
                    {
                        case "pl":
                            komunikat = new Windows.UI.Popups.MessageDialog("Coś poszło nie tak :(", "Błąd");
                        break;
                        case "de":
                            komunikat = new Windows.UI.Popups.MessageDialog("Etwas schief gegangen :(", "Fehlermeldung");
                        break;
                        default:
                            komunikat = new Windows.UI.Popups.MessageDialog("something went wrong :(", "Error");
                        break;
                    }
                    await komunikat.ShowAsync();
                    return;
                }
            }

        }

        private void przedmiot_KeyDown(object sender, KeyRoutedEventArgs e)
        {
        
        }
    }
}
