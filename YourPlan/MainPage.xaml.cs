﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourPlan.AppbarSetting;
using YourPlan.Baza;
using YourPlan.Klasy;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace YourPlan
{
    public sealed partial class MainPage : YourPlan.Common.LayoutAwarePage
    {
        Ustawienia ust = new Ustawienia();
        SQL baza;
        public MainPage()
        {
            this.InitializeComponent();
            this.DataContext = ust;
        }
        #region Domyślne
            protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState){}
            protected override void SaveState(Dictionary<String, Object> pageState){}
        #endregion

        //sterowanie scrollami
        private void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            this.sliderBok.ScrollToHorizontalOffset(Slider.HorizontalOffset);
            this.sliderPion.ScrollToVerticalOffset(Slider.VerticalOffset);
        }

        bool test = true;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           

            if (test == true)
            {
                ust.KolumnaCzM = 0;
                ust.CzW = new GridLength(0); 
            }
            else
            {
                ust.KolumnaCzM = 200;
                ust.CzW = new GridLength(1, GridUnitType.Star);
            }
            test = !test;
        }

        private async void pageRoot_Loaded(object sender, RoutedEventArgs e)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;

            try
            {
                var obj = await Open("Settings", typeof(Ustawienia));

                if (obj != null)
                {
                    (obj as Ustawienia).Przypisz();
                    ust = obj as Ustawienia; 
                }
            }
            catch (Exception) { ust.Domyslne(); }
            

            this.DataContext = ust;

            baza = new SQL();


        }

        private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var ustaw = new SettingsCommand("tutek", "Ustawienia", (handler) =>
            {
                var settings = new Callisto.Controls.SettingsFlyout()
                {
                    Width = 800,
                    ContentBackgroundBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 92, 92, 92)),
                    Content = new SettingsBar.Settings(ust),
                    HeaderText = "Ustawienia",
                    IsOpen = true
                };
            });

            args.Request.ApplicationCommands.Add(ustaw);

        }

        private async Task<Object> Open(string name, Type typ)
        {
            StorageFile file = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(name);
            }
            catch (Exception) { return null; };

            IRandomAccessStream inStream = await file.OpenReadAsync();
            XmlSerializer serializer = new XmlSerializer(typ);

            Object zwroc = (Object)serializer.Deserialize(inStream.AsStreamForRead());

            inStream.Dispose();

            return zwroc;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            glowny.Margin = new Thickness(350, 0, 30, 10);
            //glowny.Margin = new Thickness(30, 0, 30, 10);
            _frame.Content = new Dodawanie(this);
        }

       

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            _frame.Content = new Edytowanie(this);
        }

        private void appbar_Closed(object sender, object e)
        {
           
        }

        private void pageRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //TestGrid.Width = grid1.ActualWidth;
        }

    }
}
