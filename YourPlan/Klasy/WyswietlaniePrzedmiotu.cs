﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourPlan.Klasy
{
    class WyswietlaniePrzedmiotu
    {
        private String _nazwaPrzedmiotu;
        private String godzRozp;
        private String godzZak;
        private String _nauczyciel;
        private String _sala;
        private double wys;
        private Windows.UI.Xaml.Thickness margines;
        private String name;
        private bool widoczny = false;

        public String Sala                          { get { return _sala;               } set { _sala = value;              } }//property change
        public String Nauczyciel                    { get { return _nauczyciel;         } set { _nauczyciel = value;        } }
        public String GodzRozp                      { get { return godzRozp;            } set { godzRozp = value;           } }
        public String GodzZak                       { get { return godzZak;             } set { godzZak = value;            } }
        public String NazwaPrzed                    { get { return _nazwaPrzedmiotu;    } set { _nazwaPrzedmiotu = value;   } }
        public double Wysokosc                      { get { return wys;                 } set { wys = value;                } }
        public Windows.UI.Xaml.Thickness Margines   { get { return margines;            } set { margines = value;           } }
        public String Name                          { get { return name;                } set { name = value;               } } //dla x:Name numerek id z bazy
        public bool Widoczny                        { get { return widoczny;            } set { widoczny = value;           } }
        //zrobić bindowanie koloru do istniejącej klasy ustawienia
    }
}
